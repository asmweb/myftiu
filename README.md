Videoplazza puzzle!
================

The application solves the puzzle that has been given as test case

## How to run

mvn clean package && java -jar target/videoplaza-1.0-SNAPSHOT.jar /path/to/the/file.txt

***The application is a maven based project***

if maven is not installed
java -jar target/videoplaza-1.0-SNAPSHOT.jar /path/to/the/file.txt


##Algorithm
1. A file is passed as argument to the application
2. A list is created when the file is parsed and ordered with the highest price/campaign ratio
3. An initial combination is created and the numbers of that combination are saved in an array
4. A reshuffling of the indexes of the saved array are performed in order to find a possible better solution
5. For every reshuffle if that combination is lower than the best found combination, that combination is thrown away
6. Algorithm ends when all the possible combinations are found
7. The output of that combination is shown on the screen


