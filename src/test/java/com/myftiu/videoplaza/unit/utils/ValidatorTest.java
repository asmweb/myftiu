package com.myftiu.videoplaza.unit.utils;

import com.myftiu.videoplaza.Videoplaza;
import com.myftiu.videoplaza.model.ClientDTO;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * @author by ali myftiu on 19/10/14.
 */

@RunWith(JUnit4.class)
public class ValidatorTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Rule
	public final ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

	@Test
	public void shouldFailOnClientConstuctor(){

		//given

		// when
		expectedException.expect(IllegalArgumentException.class);
		ClientDTO clientDTO = new ClientDTO(null, 0, 0);

		//then
	}


	@Test
	public void shouldFailOnStart() {
		//given

		//when
		expectedSystemExit.expectSystemExitWithStatus(0);
		Videoplaza.main(new String[]{});

		//then
	}



}
