package com.myftiu.videoplaza.unit.service;

import com.myftiu.videoplaza.model.ClientDTO;
import com.myftiu.videoplaza.service.FileReaderService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;


/**
 * @author by ali myftiu on 18/10/14.
 */

@RunWith(JUnit4.class)
public class FileReaderServiceEnum
{

	@Rule
	public ExpectedException exception = ExpectedException.none();


	@Test
	public void shouldParseInputLine() {
		//given
		String inputLine = "Dolor,70000,71000";

		//when
		ClientDTO clientDTO = FileReaderService.FILE.parseFile(inputLine);

		//then
		assertEquals(clientDTO, new ClientDTO("Dolor", 70000, 71000));

	}

	@Test
	public void shouldFailParsinInputLine() {
		//given
		String inputLine = "Lor em,Lipsum,71000";

		//when
		exception.expect(IllegalArgumentException.class);
		ClientDTO clientDTO = FileReaderService.FILE.parseFile(inputLine);

		//then


	}


}
