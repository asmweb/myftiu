package com.myftiu.videoplaza.unit.service;

import com.myftiu.videoplaza.model.CampaignDTO;
import com.myftiu.videoplaza.model.ClientDTO;
import com.myftiu.videoplaza.model.DataModel;
import com.myftiu.videoplaza.service.CampaignService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author by ali myftiu on 18/10/14.
 */

@RunWith(JUnit4.class)
public class CampaignServiceTest {

	private CampaignService campaignService = new CampaignService();


	@Test
	public void shouldReturnTheRightCombination() {


		//given
		DataModel dataModel = createClientsList();

		//when
		CampaignDTO campaignDTO = campaignService.createMonthlyReport(dataModel);
		assertEquals(campaignDTO.getClients().size(), dataModel.getClients().size());
		assertEquals(campaignDTO.getTotalRevenue(), 51014000);
		assertEquals(campaignDTO.getNumberOfImpressions(), 50000000);

	}


	private DataModel createClientsList(){

		List<ClientDTO> clientDTOList = new ArrayList<ClientDTO>();
		clientDTOList.add(new ClientDTO("Acme", 1,0));
		clientDTOList.add(new ClientDTO("Lorem", 2,2));
		clientDTOList.add(new ClientDTO("Ipsum", 3,2));
		clientDTOList.add(new ClientDTO("Dolor", 70000,71000));
		clientDTOList.add(new ClientDTO("Mauris", 49000000,50000000));
		return  new DataModel(clientDTOList, 50000000);

	}

}
