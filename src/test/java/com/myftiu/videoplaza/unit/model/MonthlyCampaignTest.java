package com.myftiu.videoplaza.unit.model;

import com.myftiu.videoplaza.model.CampaignDTO;
import com.myftiu.videoplaza.model.ClientDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author by ali myftiu on 18/10/14.
 */

@RunWith(JUnit4.class)
public class MonthlyCampaignTest {

	@Test
	public void shouldAddClient() {
		//given
		CampaignDTO campaignDTO = new CampaignDTO(100);
		ClientDTO clientDTO = new ClientDTO("Test", 100, 200);

		//when
		Boolean result = campaignDTO.addClient(clientDTO);

		// then
		assertTrue(result);
		assertEquals(100, campaignDTO.getNumberOfImpressions());
		assertEquals(200, campaignDTO.getTotalRevenue());

	}

}
