package com.myftiu.videoplaza.model;

import com.myftiu.videoplaza.utils.Validator;

/**
 * @author by ali myftiu on 19/10/14.
 */
public class ClientDTO implements Comparable<ClientDTO> {

		private int id = 0;
		private String customer;
		private long impressions;
		private long price;
		private double profit = 0;
		private long numberOfImpressions = 0;



	public ClientDTO(String customer, long impressions, int price)
	{
		Validator.validate(customer, impressions, price);
		this.customer = customer;
		this.impressions = impressions;
		this.price = price;
		this.profit = ((double)price / impressions);
	}

	public String getCustomer()
	{
		return customer;
	}



	public long getImpressions()
	{
		return impressions;
	}



	public long getPrice()
	{
		return price;
	}



	public double getProfit()
	{
		return profit;
	}



	public int compareTo(ClientDTO o)
	{
		return Double.compare(o.profit, this.profit);
	}

	public void setCustomer(String customer)
	{
		this.customer = customer;
	}

	public void setImpressions(long impressions)
	{
		this.impressions = impressions;
	}

	public void setPrice(long price)
	{
		this.price = price;
	}


	public long getNumberOfImpressions()
	{
		return numberOfImpressions;
	}

	public void setNumberOfImpressions(long numberOfImpressions)
	{
		this.numberOfImpressions = numberOfImpressions;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String toString()
	{

		return  customer  + ", " +
				numberOfImpressions  + ", " +
				impressions + ", " +
				price + " \n";

	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		ClientDTO clientDTO = (ClientDTO) o;

		if (id != clientDTO.id)
		{
			return false;
		}
		if (impressions != clientDTO.impressions)
		{
			return false;
		}
		if (numberOfImpressions != clientDTO.numberOfImpressions)
		{
			return false;
		}
		if (price != clientDTO.price)
		{
			return false;
		}
		if (Double.compare(clientDTO.profit, profit) != 0)
		{
			return false;
		}
		if (customer != null ? !customer.equals(clientDTO.customer) : clientDTO.customer != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result;
		long temp;
		result = id;
		result = 31 * result + (customer != null ? customer.hashCode() : 0);
		result = 31 * result + (int) (impressions ^ (impressions >>> 32));
		result = 31 * result + (int) (price ^ (price >>> 32));
		temp = Double.doubleToLongBits(profit);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (int) (numberOfImpressions ^ (numberOfImpressions >>> 32));
		return result;
	}
}
