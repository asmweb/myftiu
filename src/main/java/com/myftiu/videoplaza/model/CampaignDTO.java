package com.myftiu.videoplaza.model;

import com.myftiu.videoplaza.utils.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by ali myftiu on 19/10/14.
 */
public class CampaignDTO
{

	private List<ClientDTO> clients;
	private long monthlyInventory = 0;
	private long numberOfImpressions = 0;
	private long totalRevenue = 0;


	public CampaignDTO(long monthlyInventory)
	{
		Validator.validate(monthlyInventory);
		clients = new ArrayList();
		this.monthlyInventory = monthlyInventory;
		numberOfImpressions = 0;
		totalRevenue = 0;
	}

	public CampaignDTO(List<ClientDTO> clients, long monthlyInventory)
	{
		Validator.validate(clients, monthlyInventory);
		this.clients = clients;
		this.monthlyInventory = monthlyInventory;
	}

	private void setNumberOfImpressions(long numberOfImpressions)
	{
		this.numberOfImpressions = numberOfImpressions;
	}

	private void setTotalRevenue(long totalRevenue)
	{
		this.totalRevenue = totalRevenue;
	}

	public List<ClientDTO> getClients()
	{
		return clients;
	}

	public long getMonthlyInventory()
	{
		return monthlyInventory;
	}

	public long getNumberOfImpressions()
	{
		return numberOfImpressions;
	}

	public long getTotalRevenue()
	{
		return totalRevenue;
	}

	@Override
	public String toString()
	{
		return clients + " " +
				numberOfImpressions +
				", " + totalRevenue +
				" \n";
	}




	private boolean canAddToReport(long impressions){
		return (impressions + getNumberOfImpressions() <= getMonthlyInventory());
	}

	private void update(long impressions, long price) {
		setNumberOfImpressions(impressions);
		setTotalRevenue(price);
	}

	public boolean addClient(ClientDTO client){
		boolean isClientAdded = false;
		if(canAddToReport(client.getImpressions())){
			clients.add(client);
			update(getNumberOfImpressions()+client.getImpressions(), getTotalRevenue()+client.getPrice());
			isClientAdded = true;
		}
		return isClientAdded;
	}
}
