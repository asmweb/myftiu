package com.myftiu.videoplaza.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author by ali myftiu on 19/10/14.
 */
public class DataModel {

	private List<ClientDTO> clients;
	private long monthlyInventory;


	public DataModel(List<ClientDTO> clients, long monthlyInventory)
	{
		this.clients = clients;
		this.monthlyInventory = monthlyInventory;
	}

	public List<ClientDTO> getClients()
	{
		return clients;
	}

	public long getMonthlyInventory()
	{
		return monthlyInventory;
	}

	@Override
	public String toString()
	{
		return "DataModel{" +
				"clients=" + clients +
				", monthlyInventory=" + monthlyInventory +
				'}';
	}
}
