package com.myftiu.videoplaza;

import com.myftiu.videoplaza.model.*;
import com.myftiu.videoplaza.service.CampaignService;
import com.myftiu.videoplaza.service.DeliverService;
import com.myftiu.videoplaza.service.FileReaderService;
import com.myftiu.videoplaza.utils.Validator;


import java.io.File;

/**
 * @author by ali myftiu on 18/10/14.
 */
public class Videoplaza {


	public static void main(String[] args) {

		Videoplaza videoplaza = new Videoplaza();
		Validator.validate(args.length);
		videoplaza.runService(args[0]);

	}


	private void runService(String fileName) {

		CampaignService campaignService = new CampaignService();
		File inputFile = new File(fileName);
		Validator.validate(inputFile.canRead());

		DataModel dataModel = FileReaderService.FILE.readFile(inputFile);
		CampaignDTO bestSolution = campaignService.createMonthlyReport(dataModel);
		DeliverService.DELIVER.printOutput(bestSolution);


	}



}
