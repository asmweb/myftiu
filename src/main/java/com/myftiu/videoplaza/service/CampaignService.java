package com.myftiu.videoplaza.service;

import com.myftiu.videoplaza.model.CampaignDTO;
import com.myftiu.videoplaza.model.ClientDTO;
import com.myftiu.videoplaza.model.DataModel;
import com.myftiu.videoplaza.utils.Validator;


import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author by ali myftiu on 18/10/14.
 */


public class CampaignService {

	private long[] desiredCombination;
    private DataModel currentDataModel;
	private long[] impressionsArray;
	private long[] priceArray;
	private double[] profitArray;
	private long shuffleCalls = 0;

	public CampaignService() {}


	public CampaignDTO createMonthlyReport(DataModel dataModel) {

		long lStartTime = System.currentTimeMillis();
        Validator.validate(dataModel);

		this.currentDataModel = dataModel;
		long currentCombinationRevenue;
		long currentCombinationImpressions;
		long nextCombinationRevenue = 0;
		long monthlyInventory = dataModel.getMonthlyInventory();
		desiredCombination = new long[dataModel.getClients().size()];
		long[] actualCombination = new long[desiredCombination.length];



		/* an initial combination is created where the items with the highest profitArray ratio are inserted  */
		createInitialCombination(dataModel.getClients(), dataModel.getMonthlyInventory());


        long bestCombinationRevenue = getTotalRevenue(this.desiredCombination);

		/* a backup copy for this solution is created anyway */
		System.arraycopy(desiredCombination, 0, actualCombination, 0, desiredCombination.length);

		/* the algorithm checks for the next best combination that returns the highest revenue */
		boolean branch = true;
		while (branch) {

			int nextIndexToRemove = findIndexToRemove(actualCombination);

			/* a campaign is removed from the monthly report
			 * so that we can add a campaign from the next most important
			 * client.
			 */
			actualCombination[nextIndexToRemove] -= 1;

			currentCombinationRevenue = getTotalRevenue(actualCombination);
			currentCombinationImpressions = getTotalImpressions(actualCombination);


			if ((nextIndexToRemove + 1) < dataModel.getClients().size()) {

				/* a potential profitArray is calculated in order to make a decision whether a better combination is found */
				long remainingImpressions = (monthlyInventory - currentCombinationImpressions);
				double potentialProfit = remainingImpressions * profitArray[nextIndexToRemove + 1];

				/* did we find a better combination that gives a higher revenue */
				if (currentCombinationRevenue + potentialProfit > bestCombinationRevenue) {
					createNextCombination(actualCombination, monthlyInventory, nextIndexToRemove);
					nextCombinationRevenue = getTotalRevenue(actualCombination);
				}
			}

			/* if a better revenue was found or not with no more than three adds */
			if (nextCombinationRevenue > bestCombinationRevenue) {

				System.arraycopy(actualCombination, 0, desiredCombination, 0, actualCombination.length);
				bestCombinationRevenue = nextCombinationRevenue;

			}

			/* we go over and over until all the possible combinations have been tried */
			if (isCombinationOver(actualCombination)) {
				branch = false;
			}
		}

		long difference = System.currentTimeMillis() - lStartTime;
		System.out.println("Shuffle calls are " + shuffleCalls);
		System.out.println("Elapsed sec:  " + TimeUnit.MILLISECONDS.toSeconds(difference));

		return createMonthlyCampaign(monthlyInventory);
	}


	private void createInitialCombination(List<ClientDTO> clients, long monthlyInventory) {

		impressionsArray = new long[desiredCombination.length];
		priceArray = new long[desiredCombination.length];
		profitArray = new double[desiredCombination.length];
		long currentSize = 0;
		int pos = 0;

		Collections.sort(clients);

		for (ClientDTO client : clients) {
			desiredCombination[pos] = 0;
			while ((currentSize + client.getImpressions()) <= monthlyInventory) {
				desiredCombination[pos] += 1;
				currentSize += client.getImpressions();
			}
			impressionsArray[pos] = client.getImpressions();
			priceArray[pos] = client.getPrice();
			profitArray[pos] = client.getProfit();

			pos++;
		}
	}


	private void createNextCombination(long[] actualCombination, long monthlyInventory, int nextIndex) {
		long impressions = getTotalImpressions(actualCombination);
		for (int i = nextIndex + 1; i < actualCombination.length; i++) {
			ClientDTO clientDTO = currentDataModel.getClients().get(i);
			while ((impressions + clientDTO.getImpressions()) <= monthlyInventory) {
					actualCombination[i]++;
				    impressions = impressionsArray[i] + impressions;
			}
		}
	}


	private int findIndexToRemove(long[] actualCombination) {

		int nextPosition = 0;
		for (int i = (actualCombination.length - 1); i >= 0; i--) {
			if (actualCombination[i] > 0) {
				nextPosition = i;
				shuffleCalls++;
				break;
			}
		}

		return nextPosition;
	}





	private long getTotalImpressions(long[] actualCombination) {

		long impressionsNr = 0;

			for (int i = 0; i < actualCombination.length; i++) {
				impressionsNr += actualCombination[i] * impressionsArray[i];
			}
		return impressionsNr;
	}



	private long getTotalRevenue(long[] actualCombination) {
		long totalRevenue = 0;

			for (int i = 0; i < actualCombination.length; i++) {
				totalRevenue += actualCombination[i] * priceArray[i];
			}

		return totalRevenue;
	}


	/**
	 * every previous combination is discarded; the shuffle ends when the array contains only zero elements
	 *
	 * @param actualCombination
	 * @return true/false depending on whether all the elements were removed from the array
	 */
	private boolean isCombinationOver(long[] actualCombination) {
		boolean combinationOver = true;
		for (int i = 0; i < actualCombination.length; i++) {
			if (actualCombination[i] > 0) {
				combinationOver = false;
				break;
			}
		}
		return combinationOver;
	}


	private CampaignDTO createMonthlyCampaign(long monthlyInventory) {
		CampaignDTO dataDTO = new CampaignDTO(monthlyInventory);
		for (int i = 0; i < desiredCombination.length; i++) {
			ClientDTO client = currentDataModel.getClients().get(i);
			client.setImpressions(client.getImpressions() * desiredCombination[i]);
			client.setPrice(client.getPrice() * desiredCombination[i]);
			client.setNumberOfImpressions(desiredCombination[i]);
			dataDTO.addClient(client);
		}
		return dataDTO;
	}


}
