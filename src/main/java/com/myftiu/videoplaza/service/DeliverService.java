package com.myftiu.videoplaza.service;

import com.myftiu.videoplaza.model.CampaignDTO;
import com.myftiu.videoplaza.model.ClientDTO;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author by ali myftiu on 19/10/14.
 */
public enum  DeliverService {

	DELIVER;


	public void printOutput(CampaignDTO campaignDTO) {

		StringBuilder sb = new StringBuilder();

		restoreOriginalOrder(campaignDTO.getClients());

		for(ClientDTO client : campaignDTO.getClients()) {
			sb.append(client.getCustomer() + "," + client.getNumberOfImpressions() + "," + client.getImpressions() + "," + client.getPrice() + "\n");
		}
		sb.append(campaignDTO.getNumberOfImpressions() + "," + campaignDTO.getTotalRevenue());
		System.out.println(sb);


	}

	private void restoreOriginalOrder(List<ClientDTO> clientDTOList) {
		Collections.sort(clientDTOList, new Comparator<ClientDTO>()
		{
			public int compare(ClientDTO o1, ClientDTO o2)
			{
				if(o1.getId() == o2.getId()) {
					return 0;
				}
				return o1.getId() < o2.getId() ? -1 : 1;
			}
		});
	}

}


