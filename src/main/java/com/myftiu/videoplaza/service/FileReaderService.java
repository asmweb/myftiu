package com.myftiu.videoplaza.service;

import com.myftiu.videoplaza.model.ClientDTO;
import com.myftiu.videoplaza.model.DataModel;
import com.myftiu.videoplaza.utils.Validator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * @author by ali myftiu on 19/10/14.
 */
public enum FileReaderService
{

	FILE;

	private static final Pattern PATTERN = Pattern.compile(("(?:[a-z][a-z]+)(,)(\\d+)(,)(\\d+)"),Pattern.CASE_INSENSITIVE);


	public DataModel readFile(File inputFile) {

		Validator.validate(inputFile);

		long monthlyInventory = 0;
		List<ClientDTO> clientDTOList = new ArrayList();

		FileInputStream fstream = null;
		DataInputStream in = null;
		try {

			fstream = new FileInputStream(inputFile);
			in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			monthlyInventory = Long.parseLong(br.readLine());

			String strLine;
			int index = 0;
			while ((strLine = br.readLine()) != null) {
				ClientDTO clientDTO = this.parseFile(strLine);
				index++;
				clientDTO.setId(index);
				if (clientDTO != null) {
					clientDTOList.add(clientDTO);
				} else {
					throw new IllegalArgumentException("The file is not correct");
				}
			}
		} catch (Exception e) {
			System.err.println("Error during reading input file: " + e.getMessage());
		} finally {
			try
			{
				in.close();
				fstream.close();
			}
			catch (IOException e)
			{
				System.err.println("Error while closing the streams: " + e.getMessage());
			}
		}

		DataModel dataModel = new DataModel(clientDTOList, monthlyInventory);
		

		return dataModel;
	}


	public ClientDTO parseFile(String inputLine) {

		Scanner lineScanner = new Scanner(inputLine.trim());
		if(lineScanner.hasNext(PATTERN)) {
			lineScanner.useDelimiter(",");
			String customer = lineScanner.next();
			long impressions = lineScanner.nextLong();
			int price = lineScanner.nextInt();
			return new ClientDTO(customer, impressions, price);
		} else {
			throw new IllegalArgumentException("Incorrect file format");
		}
	}

}
