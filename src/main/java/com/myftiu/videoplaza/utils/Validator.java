package com.myftiu.videoplaza.utils;

import com.myftiu.videoplaza.model.ClientDTO;
import com.myftiu.videoplaza.model.DataModel;

import java.io.File;
import java.util.List;

/**
 * @author by ali myftiu on 19/10/14.
 *
 */
public class Validator {


	public static void validate(String customer, long impressions, int price) {

		if(customer == null){
			throw new IllegalArgumentException("Please check that there is customer for campaign");
		}


		if(impressions < 1) {
			throw new IllegalArgumentException("Please make sure that the impressions are correct");
		}

		if(price < 0 ) {
			throw new IllegalArgumentException("Please make sure that the price is correct");
		}

	}


	public static void validate(List<ClientDTO> clients, long monthlyInventory) {

		if(clients == null){
			throw new IllegalArgumentException("Please that there are clients in the list");
		}

		if(monthlyInventory < 0) {
			throw new IllegalArgumentException("Please make sure that monthly inventory is correct");
		}

	}


	public static void validate(long monthlyInventory) {
		if(monthlyInventory < 0) {
			throw new IllegalArgumentException("MonthlyInventory can not be minor than zero");
		}
	}


	public static void validate(File inputFile) {
		if(inputFile == null){
			throw new IllegalArgumentException("There was no input file");
		}
	}


	public static void validate(int argsNumber) {
		if (argsNumber != 1) {
			System.out.println(" ------ No file has been specified --------");
			System.exit(0);
		}
	}

	public static void validate(boolean value) {
		if(value == false) {
			System.out.println("The input file can not be read.");
			System.exit(0);
		}
	}

    public static void validate(DataModel dataModel) {
        if(dataModel == null){
            throw new IllegalArgumentException("Please make sure that the parameter datamodel is correct");
        }
    }

}
